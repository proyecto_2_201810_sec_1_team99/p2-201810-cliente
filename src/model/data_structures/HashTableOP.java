package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

public class HashTableOP<Key, Value> implements IHashTable<Key, Value>
{
	public static final int INIT_CAPACITY = 36000;
	
	//Atributos
	private int n_numElementos;
	private int m_size;
	private Key[] keys;
	private Value[] values;
	//Constructor que inicializa una tabla vacia
	public HashTableOP()
	{
		this(INIT_CAPACITY);
	}
	//Constructor que inicializa una tabla vacia con una capacidad inicial
	public HashTableOP(int initCapacity) 
	{
		n_numElementos = 0;
		m_size = initCapacity;
		keys = (Key[]) new Object[m_size];
		values = (Value[]) new Object[m_size];
	}
	//Metodos
	/**
	 * 
	 * @return
	 */
	public int size()
	{
		return n_numElementos;
	}
	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		return size() == 0;
	}
	/**
	 * 
	 * @param key
	 * @return
	 */
	public boolean contains(Key key)
	{
		if(key == null)
			throw new NullPointerException();
		return get(key) != null;
	}
	/**
	 * 
	 * @param key
	 * @return
	 */
	public int hash(Key key)
	{
		return (key.hashCode() & 0x7fffffff) % m_size;
	}
	/**
	 * 
	 */
	public void rehash(int capacity)
	{
		HashTableOP<Key, Value> temp = new HashTableOP<Key, Value>(capacity);
		for(int i = 0; i < m_size; i++)
		{
			if(keys[i] != null)
				temp.put(keys[i], values[i]);
		}
		keys = temp.keys;
		values = temp.values;
		m_size = temp.m_size;
	}
	/**
	 * 
	 */
	@Override
	public void put(Key key, Value value) 
	{
		if(key == null)
			throw new NullPointerException();
		if(value == null)
		{
			delete(key);
			return;
		}
		//Duplica la hash table si el 50% de ella est� llena
		if(n_numElementos >= m_size/2)
			rehash(2*m_size);
		int i;
		for(i = hash(key); keys[i] != null; i = (i +1) % m_size)
		{
			if(keys[i].equals(key))
			{
				values[i] = value;
				return;
			}
		}
		keys[i] = key;
		values[i] = value;
		n_numElementos++;
	}
	/**
	 * 
	 */
	@Override
	public Value get(Key key) 
	{
		if(key == null)
			throw new NullPointerException();
		for(int i = hash(key); keys[i] != null; i = (i+1) % m_size)
		{
			if(keys[i].equals(key))
				return values[i];
		}
		return null;

	}
	/**
	 * 
	 */
	@Override
	public Value delete(Key key) 
	{
		if(!isEmpty())
		{
			if(key == null)
				throw new NullPointerException();
			else
			{
				for(int i = 0; i < m_size; i++)
				{
					if(keys[i].equals(key))
					{
						n_numElementos--;
						return values[i];
					}
				}
			}
		}    
        return null;
	}
	/**
	 * 
	 */
	@Override
	public Iterable<Key> keys() 
	{
		// TODO Auto-generated method stub
		LinkedQueue<Key> queue = new LinkedQueue<Key>();
        for (int i = 0; i < m_size; i++)
            if (keys[i] != null) queue.enqueue(keys[i]);
        return (Iterable<Key>) queue;
	}

	@Override
	public int getHashCode(Key pKey) 
	{
		// TODO Auto-generated method stub
		int hashCode = pKey.hashCode();
		int index = hashCode % m_size;
		return index;
	}
	
}
