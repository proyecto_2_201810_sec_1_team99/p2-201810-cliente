package model.data_structures;

public class HashNode<Key,Value>
{
	//Atributos
	private Key key;
	private Value value;
	private HashNode<Key,Value> next;
	//Constructor
	public HashNode(Key pKey, Value pValue, HashNode<Key, Value> pNext)
	{
		key = pKey;
		value = pValue;
		next = pNext;
	}
	//Metodos
	public Key getKey()
	{
		return key;
	}
	public Value getValue()
	{
		return value;
	}
	public HashNode<Key,Value> getNext()
	{
		return next;
	}

	public void setValue(Value value) 
	{
		this.value = value;
	}
	public void setNext(HashNode<Key,Value> next) 
	{
		this.next = next;
	}
}
