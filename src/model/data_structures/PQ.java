package model.data_structures;

import java.util.Iterator;

public class PQ<T extends Comparable<T>, E extends Comparable<E>> implements IPQ<T, E>
{
	private PriorityNode<T, E> end;
	private int size;
	public enum PriorityMode
	{
		MIN,MAX
	}

	public PQ()
	{
		end = null;
		size = 0;
	}

	@Override
	public void push(T item, E priority, PriorityMode p) 
	{
		if(p == PriorityMode.MIN)
		{
			PriorityNode<T, E> n = new PriorityNode<T, E>(item, priority);
			if(this.isEmpty())
			{
				end = n;
				size++;
			}
			else
			{
				if(this.end.getPriority().compareTo(priority) > 0)
				{
					n.setNext(this.end);
					this.end = n;
					size++;
				}
				else
				{
					PriorityNode<T, E> temp = this.end;
					while(temp.getNextNode() != null && ((PriorityNode<T, E>)temp.getNextNode()).getPriority().compareTo(priority) < 0)
					{
						temp = ((PriorityNode<T, E>)temp.getNextNode());
					}
					n.setNext((PriorityNode<T, E>)temp.getNextNode());
					temp.setNext(n);
					size++;
				}
			}
		}
		else
		{
			PriorityNode<T, E> n = new PriorityNode<T, E>(item, priority);
			if(this.isEmpty())
			{
				end = n;
				size++;
			}
			else
			{
				if(this.end.getPriority().compareTo(priority) < 0)
				{
					n.setNext(this.end);
					this.end = n;
					size++;
				}
				else
				{
					PriorityNode<T, E> temp = this.end;
					while(temp.getNextNode() != null && ((PriorityNode<T, E>)temp.getNextNode()).getPriority().compareTo(priority) > 0)
					{
						temp = ((PriorityNode<T, E>)temp.getNextNode());
					}
					n.setNext((PriorityNode<T, E>)temp.getNextNode());
					temp.setNext(n);
					size++;
				}
			}
		}
	}

	@Override
	public T pop() 
	{
		T e = null;
		if (end != null)
		{
			e = end.getElement();
			end = ((PriorityNode<T, E>) end.getNextNode());
			size--;
		}
		return e;
	}

	@Override
	public boolean isEmpty() 
	{
		return (size == 0);
	}
	
	public int size()
	{
		return size;
	}

	/**
	 * Método que obtiene el elemento al inicio de la estructura sin removerlo.
	 * @return El elemento al inicio de la pila.
	 */
	public T peek()
	{
		return (end == null) ? null : end.getElement();
	}

	/**
	 * Devuelve un iterador sobre la estructura.
	 * El iterador empieza en el primer elemento
	 * @return un nuevo iterador sobre la estructura.
	 */
	public Iterator<T> iterator() 
	{
		return new SimpleIterator<T>(end);
	}
}
