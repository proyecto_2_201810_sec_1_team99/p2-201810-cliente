package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

public class HashTableSC<Key, Value> implements IHashTable<Key, Value>
{
    private static final int INIT_CAPACITY = 64;

    private int n_numElemnetos;                          
    private int m_size;                                
    private BusquedaSecuencial<Key, Value>[] st;  
    
    /**
     * Initializes an empty symbol table.
     */
    public HashTableSC() 
    {
        this(INIT_CAPACITY);
    } 

    /**
     * Initializes an empty symbol table with {@code size} chains.
     * @param m the initial number of chains
     */
    public HashTableSC(int size) 
    {
        this.m_size = size;
        st = (BusquedaSecuencial<Key, Value>[]) new BusquedaSecuencial[m_size];
        for (int i = 0; i < m_size; i++)
        {
        	st[i] = new BusquedaSecuencial<Key, Value>();
        }
    }

    // resize the hash table to have the given number of chains,
    // rehashing all of the keys
    private void resize(int chains) 
    {
        HashTableSC<Key, Value> temp = new HashTableSC<Key, Value>(chains);
        for (int i = 0; i < m_size; i++) 
        {
            for (Key key : st[i].keys()) 
            {
                temp.put(key, st[i].get(key));
            }
        }
        this.m_size  = temp.m_size;
        this.n_numElemnetos  = temp.n_numElemnetos;
        this.st = temp.st;
    }

    // hash value between 0 and m-1
    private int hash(Key key) 
    {
        return (key.hashCode() & 0x7fffffff) % m_size;
    } 

    /**
     * Returns the number of key-value pairs in this symbol table.
     *
     * @return the number of key-value pairs in this symbol table
     */
    public int size() 
    {
        return n_numElemnetos;
    } 

    /**
     * Returns true if this symbol table is empty.
     *
     * @return {@code true} if this symbol table is empty;
     *         {@code false} otherwise
     */
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * Returns true if this symbol table contains the specified key.
     *
     * @param  key the key
     * @return {@code true} if this symbol table contains {@code key};
     *         {@code false} otherwise
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public boolean contains(Key key) {
        if (key == null)
        {
        	throw new IllegalArgumentException("argument to contains() is null");
        }
        return get(key) != null;
    } 

    /**
     * Returns the value associated with the specified key in this symbol table.
     *
     * @param  key the key
     * @return the value associated with {@code key} in the symbol table;
     *         {@code null} if no such value
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public Value get(Key key) {
        if (key == null) throw new IllegalArgumentException("argument to get() is null");
        int i = hash(key);
        return st[i].get(key);
    } 

    /**
     * Inserts the specified key-value pair into the symbol table, overwriting the old 
     * value with the new value if the symbol table already contains the specified key.
     * Deletes the specified key (and its associated value) from this symbol table
     * if the specified value is {@code null}.
     *
     * @param  key the key
     * @param  val the value
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public void put(Key key, Value val) {
        if (key == null) throw new IllegalArgumentException("first argument to put() is null");
        if (val == null) {
            delete(key);
            return;
        }

        // double table size if average length of list >= 10
        if (n_numElemnetos >= 10*m_size) resize(2*m_size);

        int i = hash(key);
        if (!st[i].contiene(key))
        	n_numElemnetos++;
        st[i].put(key, val);
    } 

    /**
     * Removes the specified key and its associated value from this symbol table     
     * (if the key is in this symbol table).    
     *
     * @param  key the key
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public Value delete(Key key) 
    {
        if (key == null) throw new IllegalArgumentException("argument to delete() is null");

        int i = hash(key);
        if (st[i].contiene(key))
        	n_numElemnetos--;
        st[i].delete(key);

        // halve table size if average length of list <= 2
        if (m_size > INIT_CAPACITY && n_numElemnetos <= 2*m_size) resize(m_size/2);
        return null;
    } 

    // return keys in symbol table as an Iterable
    public Iterable<Key> keys() 
    {
        LinkedQueue<Key> queue = new LinkedQueue<Key>();
        for (int i = 0; i < m_size; i++) 
        {
            for (Key key : st[i].keys())
                queue.enqueue(key);
        }
        return (Iterable<Key>) queue;
    } 
	@Override
	public int getHashCode(Key pKey) 
	{
		// TODO Auto-generated method stub
		int hashCode = pKey.hashCode();
		int index = hashCode % m_size;
		return index;
	}

	@Override
	public void rehash(int pCap) {
		// TODO Auto-generated method stub
		m_size = pCap;
	}
	
}
