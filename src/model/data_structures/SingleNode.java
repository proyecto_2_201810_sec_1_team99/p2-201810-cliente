package model.data_structures;

public class SingleNode<T> 
{
	/**
	 * Elemento que se almacena en el nodo.
	 */
	protected T element;
	
	/**
	 * El siguiente nodo
	 */
	protected SingleNode<T> nextNode;
	
	/**
	 * Constructor del nodo.
	 * @param element El elemento a almacenar en el nodo. element != null.
	 */
	public SingleNode(T element)
	{
		this.element = element;
	}
	
	/**
	 * M�todo que cambia el siguiente nodo.
	 * @param next El nuevo nodo siguiente.
	 */
	public void setNext(SingleNode<T> next)
	{
		this.nextNode = next;
	}
	
	/**
	 * M�todo que devuelve el elemento que almacena el nodo.
	 * @return El elemento contenido en el nodo.
	 */
	public T getElement()
	{
		return element;
	}
	
	/**
	 * Cambia el elemento almacenado en el nodo.
	 * @param element El nuevo elemento a almacenar.
	 */
	public void setElement(T element)
	{
		this.element = element;
	}
	
	
	/**
	 * M�todo que retorna el siguiente nodo.
	 * @return Siguiente nodo.
	 */
	public SingleNode<T> getNextNode()
	{
		return nextNode;
	}
	
	/**
	 * M�todo toString sobreescrito para identificar el elemento.
	 */
	@Override
	public String toString()
	{
		return element.toString();
	}
}
