package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add: add a new element T 
 * delete: delete the given element T 
 * get: get the given element T (null if it doesn't exist in the list)
 * size: return the the number of elements
 * get: get an element T by position (the first position has the value 0) 
 * listing: set the listing of elements at the firt element
 * getCurrent: return the current element T in the listing (return null if it doesn�t exists)
 * next: advance to next element in the listing (return if it exists)
 * @param <T>
 */
public interface LinkedList <T extends Comparable<T>> extends Iterable<T>
{
	/**
	 * Método que añade un elemento a la lista.
	 * @return True si se a�adio el elemento, false en caso contrario.
	 */
	public boolean addLast(T element);
	
	/**
	 * M�todo que a�ade un elemento a la lista.
	 * @param element Elemento a a�adir.
	 * @return true si se a�adio, false de lo contrario.
	 */
	public boolean addFirst(T element);
	
	
	/**
	 * Método que elimina un elemento de la lista según el elemento dado.
	 * @param id Identificador del elemento a eliminar.
	 */
	public boolean delete(Object o);
	
	/**
	 * Método que obtiene un elemento de la lista segun el elemento dado.
	 * @param id El identificador del elemento a obtener.
	 * @return El elemento obtenido.
	 */
	public T get(Object o);
	
	/**
	 * Método que retorna el tamaño de la lista.
	 * @return El tamaño de la lista.
	 */
	public int size();
	
	/**
	 * Método que retorna el elemento buscado según un indice.
	 * @param index El indice del elemento a obtener.
	 * @return El elemento a obtener.
	 */
	public T get (int index);
	
	/**
	 * Método que pone el listado de elementos en el primer elemento.
	 */
	public void listing();
	
	/**
	 * Método que obtiene el elemento actual del listado.
	 * @return El elemento actual del listado.
	 */
	public T getCurrent();
	
	/**
	 * Método que avanza al siguiente elemento en el listado.
	 * @return True si existe, false de lo contrario.
	 */
	public boolean next();
	
}
