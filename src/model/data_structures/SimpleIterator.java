package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class SimpleIterator<T extends Comparable <T>> implements Iterator<T> 
{
	/**
	 * El nodo donde se encuentra el iterador.
	 */
	private SingleNode<T> actual;

	/**
	 * Crea un nuevo iterador iniciando en el nodo indicado
	 * @param primerNodo el nodo donde se desea que inicie el iterador
	 */
	public SimpleIterator(SingleNode<T> first) 
	{
		actual = first;
	}

	/**
	 * Indica si a�n hay elementos por recorrer
	 * @return true en caso de que  a�n haya elemetos o false en caso contrario
	 */
	public boolean hasNext() 
	{
		return actual != null;
	}

	/**
	 * Devuelve el siguiente elemento a recorrer
	 * <b>post:</b> se actualizado actual al siguiente del actual
	 * @return objeto en actual
	 */
	public T next() 
	{
		if(!hasNext())
			throw new NoSuchElementException();
		T value = actual.getElement();
		actual = actual.getNextNode();

		return value;
	}

}
