package model.data_structures;

public class BusquedaSecuencial<Key, Value> {

	private int n;
	private HashNode primero;
	
	public BusquedaSecuencial( )
	{
		
	}
	
	public int size( )
	{
		return n;
	}
	
	public boolean isEmpty( )
	{
		return size( ) == 0;
	}
	
	public boolean contiene(Key key)
	{
		if(key == null)
		{
			throw new IllegalArgumentException("El argumento de llave se encuentra vacio");
		}
		
		return get(key) != null;
	}
	
	public Value get(Key key)
	{
		if(key == null)
		{
			throw new IllegalArgumentException("El argumento de llave se encuentra vacio");
		}
		
		for(HashNode<Key, Value> i = primero; i != null; i = i.getNext( ))
		{
			if(key.equals(i.getKey( )))
			{
				return i.getValue( );
			}
		}
		
		return null;
	}
	
	public void put(Key key, Value val) 
	{
        if (key == null) 
        	{
        		throw new IllegalArgumentException("El argumento de llave se encuentra vacio"); 
        	}
        if (val == null)
        {
            delete(key);
            return;
        }

        for (HashNode<Key, Value> i = primero; i != null; i = i.getNext()) 
        {
            if (key.equals(i.getKey()))
            {
                i.setValue(val);
                return;
            }
        }
        primero = new HashNode<Key, Value>(key, val, primero);
        n++;
    }
	
	public void delete(Key key) 
	{
        if (key == null)
        	{
        		throw new IllegalArgumentException("El argumento de llave se encuentra vacio"); 
        	} 
        primero = delete(primero, key);
    }
	
	private HashNode<Key, Value> delete(HashNode<Key, Value> i, Key key)
	{
		if (i == null) return null;
		if (key.equals(i.getKey( )))
		{
			n--;
			return i.getNext();
		}
		
		i.setNext(delete(i.getNext(), key));
	    return i;
	 }
	
	 public Iterable<Key> keys()  {
	        LinkedQueue<Key> queue = new LinkedQueue<Key>();
	        for (HashNode<Key, Value> i = primero; i != null; i = i.getNext( ))
	            queue.enqueue(i.getKey());
	        return (Iterable<Key>) queue;
	    }
}
