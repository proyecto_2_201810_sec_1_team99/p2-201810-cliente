package model.data_structures;

public class SingleLinkedList <T extends Comparable<T>> extends AbstractList<T>
{

	/**
	 * Nodo del final
	 */
	private SingleNode<T> lastNode;

	/**
	 * Nodo de enlistamiento
	 */
	private SingleNode<T> listingNode;

	/**
	 * Construye una lista vacia
	 * <b>post:< /b> se ha inicializado el primer nodo en null
	 */
	public SingleLinkedList() 
	{
		firstNode = null;
		listingNode = null;
		lastNode = null;
		size = 0;
	}

	/**
	 * Se construye una nueva lista cuyo primer nodo  guardar� al elemento que llega por par�mentro
	 * @param nFirst el elemento a guardar en el primer nodo
	 * @throws NullPointerException si el elemento recibido es nulo
	 */
	public SingleLinkedList(T nFirst)
	{
		if(nFirst == null)
		{
			throw new NullPointerException("Se recibe un elemento nulo");
		}
		firstNode = new DoubleNode<T>(nFirst);
		lastNode = (DoubleNode<T>) firstNode;
		listingNode = null;
		size = 1;
	}

	@Override
	public boolean addLast(T element) 
	{
		boolean agregado = false;
		if(element == null)
		{
			throw new NullPointerException();
		}
		if(this.isEmpty())
		{
			this.firstNode = new SingleNode<T>(element);
			this.lastNode =  this.firstNode;
			this.size++;
			agregado = true;
		}
		else
		{
			SingleNode<T> n = new SingleNode<T>(element);
			this.lastNode.setNext(n);
			this.size++;
			this.lastNode = n;
			agregado = true;
		}
		return agregado;
	}

	@Override
	public boolean addFirst(T element) 
	{
		boolean added = false;
		if(element == null)
		{
			throw new NullPointerException();
		}

		if(this.isEmpty())
		{
			this.firstNode = new SingleNode<T>(element);
			this.lastNode = this.firstNode;
			this.size++;
			added = true;
		}
		else
		{
			SingleNode<T> n = new SingleNode<T>(element);
			n.setNext(this.firstNode);

			this.size++;
			this.firstNode = n;
			added = true;
		}
		return added;
	}

	@Override
	public boolean delete(Object o) 
	{
		boolean elimino = false;
		boolean encontro = false;

		SingleNode<T> actual =  this.firstNode;

		if(actual != null && actual.getElement().compareTo((T) o) == 0)
		{
			this.firstNode = actual.getNextNode();
			elimino = true;
			this.size--;
		}
		else if (actual!= null && actual.getNextNode() != null)
		{	
			SingleNode<T> anterior = actual;
			actual = actual.getNextNode();

			while(actual != null && !encontro)
			{
				if(actual.getElement().compareTo((T)o) == 0)
				{
					encontro = true;
				}
				else
				{
					anterior = actual;
					actual = actual.getNextNode();
				}
			}
			if(encontro)
			{
				anterior.setNext(actual.getNextNode());
				elimino = true;
				this.size--;
			}
		}
		return elimino;
	}

	@Override
	public T get(Object o) 
	{
		SingleNode<T> n = this.firstNode;
		T searched = null;
		boolean found = false;
		if(n != null)
		{
			while(n != null && !found)
			{
				if(n.getElement().compareTo((T)o) == 0)
				{
					found = true;
					searched = n.getElement();
				}
				else
				{
					n = n.getNextNode();
				}
			}
		}
		return searched;
	}

	@Override
	public void listing() 
	{
		listingNode =  this.firstNode;

	}

	@Override
	public T getCurrent() 
	{
		return listingNode.getElement();
	}

	@Override
	public boolean next() 
	{
		boolean exists = false;
		if(listingNode != null)
		{
			exists = true;
			listingNode =  listingNode.getNextNode();
		}
		return exists;
	}
}
