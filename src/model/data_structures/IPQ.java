package model.data_structures;

import model.data_structures.PQ.PriorityMode;

public interface IPQ<T, E> extends Iterable<T>
{

	/** pushes a new element at the correct level of priority*/
	public void push(T item, E priority, PriorityMode p);

	/** Dequeue the highest priority element in the PQ.
	 * @return "first" element or null if it doesn't exist
	 */
	public T pop();

	/** Evaluate if the queue is empty. 
	 * @return true if the PQ is empty. false in other case.
	 */
	public boolean isEmpty();

}
