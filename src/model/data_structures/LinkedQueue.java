package model.data_structures;

public class LinkedQueue<T> implements IQueue<T> 
{
	/**
	 * Nodo del inicio de la cola.
	 */
	private SingleNode<T> first;
	
	/**
	 * Nodo del final de la cola.
	 */
	private SingleNode<T> end;
	
	/**
	 * Tamaño de la cola.
	 */
	private int size;

	/**
	 * Constructor de la cola.
	 * post: se inicializa una cola vacía.
	 */
	public LinkedQueue() 
	{
		first = null;
		end = null;
		size = 0;
	}

	/**
	 * Método que inserta un elemento en la cola.
	 * post: se insertó el elemento en la cola.
	 */
	public void enqueue(T element) 
	{
		SingleNode<T> n = new SingleNode<T>(element);
		if (first == null) 
		{
			first = n;
			end = n;
		} 
		else 
		{
			end.setNext(n);
			end = n;
		}
		size++;
	}

	/**
	 * Remueve el elemento de la cola, retornandolo
	 * @return El elemento removido de la cola.
	 */
	public T dequeue() 
	{
		if (first == null)
		{
			return null;
		}
		else
		{
		T o = first.getElement();
		first = first.getNextNode();
		size--;
		return o;
		}
	}

	/**
	 * Verifica si la cola está vacía.
	 * @return Tru si está vacía, false de lo contrario.
	 */
	public boolean isEmpty() 
	{
		return (size == 0);
	}

	/**
	 * Retorna el tamaño de la cola.
	 * @return El tamaño de la cola.
	 */
	public int size() 
	{
		return size;
	}

	/**
	 * Método que retorna el primer elemento de la cola, sin removerlo.
	 * @return El primer elemento de la cola.
	 */
	public T first() 
	{
		if (first == null)
			return null;
		else
			return first.getElement();
	}
}
