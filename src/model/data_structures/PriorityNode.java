package model.data_structures;

public class PriorityNode <T extends Comparable<T>, E extends Comparable<E>> extends SingleNode<T>
{
	private E priority;
	
	public PriorityNode(T element, E priority)
	{
		super(element);
		this.setPriority(priority);
	}

	public E getPriority() 
	{
		return priority;
	}

	public void setPriority(E priority) 
	{
		this.priority = priority;
	}
}
