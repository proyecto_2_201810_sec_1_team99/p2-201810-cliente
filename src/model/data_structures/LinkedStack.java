package model.data_structures;

public class LinkedStack<T extends Comparable<T>> implements IStack<T>
{

	/**
	 * Nodo pricipal de la pila
	 */
	private SingleNode<T> end;
	
	/**
	 * Tama�o de la pila.
	 */
	private int size;
	
	/**
	 * Constructor de la pila
	 * post: se creo una pila vacia.
	 */
	public LinkedStack() 
	{
		end = null;
		size = 0;
	}

	/**
	 * M�todo que agrega un elemento al inicio de la pila
	 * @param element El elemento a agregar.
	 */
	public void push(T element) 
	{
		SingleNode<T> n = new SingleNode<T>(element);
		if (end == null)
		{
			end = n;
			size++;
		}
		else 
		{
			n.setNext(end);
			end = n;
			size++;
		}
	}

	/**
	 * M�todo que remueve un elemento del inicio de la pila y lo retorna.
	 * @return El elemento del inicio de la pila removido.
	 */
	public T pop() 
	{
		T e = null;
		if (end != null)
		{
			e = end.getElement();
			end = end.getNextNode();
			size--;
		}
		return e;
	}

	/**
	 * Verifica si la pila est� vac�a.
	 * @return True si est� vac�a, false de lo contrario.
	 */
	public boolean isEmpty() 
	{
		return (size == 0);
	}

	/**
	 * Retorna el tama�o de la pila.
	 * @return El tama�o de la pila.
	 */
	public int size() {

		return size;
	}

	/**
	 * M�todo que obtiene el elemento al inicio de la pila sin removerlo.
	 * @return El elemento al inicio de la pila.
	 */
	public T peek() 
	{
		T e = null;
		if (end != null)
		{
			return end.getElement();
		}
		return e;
	}
}
