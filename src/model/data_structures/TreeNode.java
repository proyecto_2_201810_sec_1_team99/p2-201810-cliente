package model.data_structures;

public class TreeNode<Key, Value> {
	
	private Key key;           
    private Value value;         
    private TreeNode<Key, Value> left, right;  
    private boolean color;     
    private int size;         

    public TreeNode(Key key, Value val, boolean color, int size) {
        this.key = key;
        this.value = val;
        this.color = color;
        this.size = size;
    }
    
    public Key getKey( )
    {
    	return key;
    }
    
    public Value getValue( )
    {
    	return value;
    }
    
    public TreeNode<Key, Value> getLeft( )
    {
    	return left;
    }

    public TreeNode<Key, Value> getRight( )
    {
    	return right;
    }
    
    public boolean getColour( )
    {
    	return color;
    }
    
    public int getSize( )
    {
    	return size;
    }
    
    public void setKey(Key key)
    {
    	this.key = key;
    }
    
    public void setValue(Value value)
    {
    	this.value = value;
    }
    
    public void setLeft(TreeNode<Key, Value> node)
    {
    	left = node;
    }
    
    public void setRight(TreeNode<Key, Value> node)
    {
    	right = node;
    }
    
    public void setColour(boolean colour)
    {
    	color = colour;
    }
    
    public void setSize(int i)
    {
    	size = i;
    }
}
