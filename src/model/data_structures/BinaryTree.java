package model.data_structures;

import java.util.NoSuchElementException;

import javax.xml.stream.events.NotationDeclaration;

//Representaci�n de un arbol rojo-negro
public class BinaryTree<Key extends Comparable<Key>,Value>
{
	private final static boolean RED = true;
	private final static boolean BLACK = false;
	
	private TreeNode<Key, Value> root;
	
	public BinaryTree( )
	{
		
	}

	private boolean isRed(TreeNode<Key, Value> node)
	{
		if(node == null)
			return false;
		else
			node.setColour(RED);
			return node.getColour();
	}
	
	private int size(TreeNode<Key, Value> node)
	{
		if(node == null)
			return 0;
		else
			return node.getSize( );
	}
	
	public int size() {
		// TODO Auto-generated method stub
		return size(root);
	}
	
	public boolean isEmpty( )
	{
		return root == null;
	}

	public Value get(Key key) 
	{
		if(key == null)
		{
			throw new IllegalArgumentException("Parametro vacio");
		}
		
		return get(root, key);
	}

	public Value get(TreeNode<Key, Value> node, Key key) 
	{
		while(node != null)
		{
			int compare = key.compareTo(node.getKey());
			if(compare < 0)
			{
				node = node.getLeft();
			}
			else if(compare > 0)
			{
				node = node.getRight();
			}
			else
			{
				return node.getValue();
			}
		}
		return null;
	}

	public boolean contains(Key key) 
	{
		return get(key) != null;
	}

	//Arbol R-N agregar
	public void put(Key key, Value value) 
	{
		// TODO Auto-generated method stub
		if(key == null)
			throw new NullPointerException();
		if(value == null)
		{
			delete(key);
			return;
		}
		root = put(root, (Key)key, (Value)value);
		root.setColour(BLACK);
	}

	private TreeNode<Key, Value> put(TreeNode<Key, Value> node, Key key, Value value)
	{
		if(node == null)
			return new TreeNode<Key, Value>(key, value, RED, 1);
		int compare = key.compareTo((Key) node.getKey());
		
		if(compare < 0)
			node.setLeft(put(node.getLeft( ), key, value));
		else if(compare > 0)
			node.setRight(put(node.getRight( ), key, value));
		else
			node.setValue(value);
		//Arregla los incovenientes de los nodos rojos
		if(isRed(node.getRight( )) && !isRed(node.getLeft( )))
			node = rotateLeft(node);
		if(isRed(node.getLeft( )) && isRed(node.getLeft().getLeft()))
			node = rotateRight(node);
		if(isRed(node.getLeft()) && isRed(node.getRight()))
			changeColors(node);
		node.setSize(size(node.getLeft()) + size(node.getRight()) + 1);
		return node;
	}
	
	//Arbol R-N eliminar
	public void deleteMin() 
	{
		if(root == null)
			throw new NoSuchElementException();
		//Si los dos hijos de la raiz son negros, cambiar la raiz a rojo
		if(!isRed(root.getLeft()) && !isRed(root.getRight()))
			root.setColour(RED);;
		root = deleteMin(root);
		if(root != null)
			root.setColour(BLACK);;
	}

	private TreeNode<Key, Value> deleteMin(TreeNode<Key, Value> node)
	{
		if(node.getLeft( ) == null)
			return null;
		if(!isRed(node.getLeft()) && !isRed(node.getLeft().getLeft()))
			node = moveRedLeft(node);
		node.setLeft(deleteMin(node.getLeft()));
		return balanceTree(node);
	}
	
	public void deleteMax() 
	{
		if(root == null)
			throw new NoSuchElementException();
		//Si los dos hijos de la raiz son negros, cambiar la raiz a rojo
		if(!isRed(root.getLeft()) && !isRed(root.getRight()))
			root.setColour(RED);
		root = deleteMax(root);
		if(root != null)
			root.setColour(BLACK);
	}
	
	private TreeNode<Key, Value> deleteMax(TreeNode<Key, Value> node)
	{
		if(isRed(node.getLeft()))
			node = rotateRight(node);
		if(node.getRight() == null)
			return null;
		if(!isRed(node.getRight()) && !isRed(node.getRight().getLeft()))
			node = moveRedRight(node);
		node.setRight(deleteMax(node.getRight()));
		return balanceTree(node);
	}

	public void delete(Key key) 
	{
		if(key == null)
			throw new NullPointerException();
		if(!contains(key))
			return;
		//Si los dos hijos de de la raiz son negros, cambiar la raiz a rojo
		if(!isRed(root.getLeft()) && !isRed(root.getRight()))
			root.setColour(RED);
		root = delete(root, key);
		if(root != null)
			root.setColour(BLACK);
	}
	
	private TreeNode<Key, Value> delete(TreeNode<Key, Value> node, Key key)
	{
		if(key.compareTo(node.getKey()) < 0)
		{
			if(!isRed(node.getLeft()) && !isRed(node.getLeft().getLeft()))
				node = moveRedRight(node);
			node.setLeft(delete(node.getLeft(), key));
		}
		else
		{
			if(isRed(node.getLeft()))
				node = rotateRight(node);
			if(key.compareTo(node.getKey()) == 0 && node.getRight() == null)
				return null;
			if(!isRed(node.getRight()) && !isRed(node.getRight().getLeft()))
				node = moveRedRight(node);
			if(key.compareTo(node.getKey()) == 0)
			{	
				TreeNode<Key, Value> auxNode = min(node.getRight());
				node.setKey(auxNode.getKey());
				node.setValue(auxNode.getValue());
				node.setRight(deleteMin(node.getRight()));
			}	
			else
				node.setRight(delete(node.getRight(), key));	
		}
		return balanceTree(node);
	}
	/**
	 * 
	 * @return
	 */
	public Key min() 
	{
        if (size() == 0.0) 
        	throw new NoSuchElementException("La tabla est� vacia");
        return (Key) min(root).getKey();
    } 
	/**
	 * 
	 * @param x
	 * @return
	 */
	private TreeNode<Key, Value> min(TreeNode<Key, Value> node) 
	{ 
        if (node.getLeft() == null) 
        	return node; 
        else                
        	return min(node.getLeft()); 
    } 
	/**
	 * 
	 * @return
	 */
	public Key max() 
	{
        if (size() == 0) 
        	throw new NoSuchElementException("La tabla est� vacia");
        return (Key) max(root).getKey( );
    } 
	/**
	 * 
	 * @param x
	 * @return
	 */
    private TreeNode<Key, Value> max(TreeNode<Key, Value> node) 
    { 
        if (node.getRight() == null)
        	return node; 
        else                 
        	return max(node.getRight()); 
    } 
	/**
	 * CLT + SHIFT der
	 * @param node
	 * @return
	 */
	private TreeNode<Key, Value> rotateRight(TreeNode<Key, Value> node)
	{
		TreeNode<Key, Value> auxNode = node.getLeft( );
		node.setLeft(auxNode.getRight());
		auxNode.setRight(node);
		auxNode.setColour(auxNode.getRight().getColour());
		auxNode.getRight().setColour(RED);
		auxNode.setSize(node.getSize());
		node.setSize(size(node.getLeft()) + size(node.getRight()) + 1);
		return auxNode;
	}
	/**
	 * CLT + SHIFT izq
	 * @param node
	 * @return
	 */
	private TreeNode<Key, Value> rotateLeft(TreeNode<Key, Value> node)
	{
		TreeNode<Key, Value> auxNode = node.getRight( );
		node.setRight(auxNode.getLeft());
		auxNode.setLeft(node);
		auxNode.setColour(auxNode.getLeft().getColour());
		auxNode.getLeft( ).setColour(RED);
		auxNode.setSize(node.getSize());
		node.setSize(size(node.getLeft()) + size(node.getRight()) + 1);
		return auxNode;
	}
	/**
	 * Cambia el color del padre y de los hijos
	 * @param node
	 */
	private void changeColors(TreeNode<Key, Value> node)
	{
		node.setColour(!node.getColour());
		node.getLeft().setColour(!node.getLeft().getColour());
		node.getRight().setColour(!node.getRight().getColour());
	}
	/**
	 * Si el padre es rojo y el izq y el hijo izq del izq son negros, uno de sus hijos a rojo
	 * @param node
	 * @return
	 */
	private TreeNode<Key, Value> moveRedLeft(TreeNode<Key, Value> node)
	{
		changeColors(node);
		if(isRed(node.getRight().getLeft()))
		{
			node.setRight(rotateRight(node.getRight()));
			node = rotateLeft(node);
			changeColors(node);
		}
		return node;
	}
	/**
	 * Si el padre es rojo y el izq y el hijo izq del der son negros, uno de sus hijos a rojo
	 * @param node
	 * @return
	 */
	private TreeNode<Key, Value> moveRedRight(TreeNode<Key, Value> node)
	{
		changeColors(node);
		if(isRed(node.getLeft().getLeft()))
		{
			node = rotateRight(node);
			changeColors(node);
		}
		return node;
	}
	/**
	 * Balancea el arbol
	 * @param node
	 * @return
	 */
	private TreeNode<Key, Value> balanceTree(TreeNode<Key, Value> node)
	{
		if(isRed(node.getRight()))
			node = rotateLeft(node);
		if(isRed(node.getLeft()))
			node = rotateRight(node);
		if(isRed(node.getLeft()) && isRed(node.getRight()))
			changeColors(node);
		node.setSize(size(node.getLeft()) + size(node.getRight()) + 1);
		return node;
	}
	/**
	 * 
	 */
	public Iterable<Key> keys() 
	{
        if (size() == 0.0) 
        	return (Iterable<Key>) new LinkedQueue<Key>();
        return keys(min(), max());
    }
	/**
	 * 
	 * @param lo
	 * @param hi
	 * @return
	 */
	public Iterable<Key> keys(Key lo, Key hi) {
        if (lo == null) 
        	throw new IllegalArgumentException("first argument to keys() is null");
        if (hi == null) 
        	throw new IllegalArgumentException("second argument to keys() is null");

        LinkedQueue<Key> queue = new LinkedQueue<Key>();
        keys(root, queue, lo, hi);
        return (Iterable<Key>) queue;
    } 
	/**
	 * 
	 * @param x
	 * @param queue
	 * @param lo
	 * @param hi
	 */
	private void keys(TreeNode<Key, Value> x, LinkedQueue<Key> queue, Key lo, Key hi) 
	{ 
        if (x == null) return; 
        int cmplo = lo.compareTo((Key) x.getKey()); 
        int cmphi = hi.compareTo((Key) x.getKey()); 
        if (cmplo < 0) keys(x.getLeft(), queue, lo, hi); 
        if (cmplo <= 0 && cmphi >= 0) queue.enqueue((Key) x.getKey()); 
        if (cmphi > 0) keys(x.getRight(), queue, lo, hi); 
    } 

}
