package model.data_structures;


public class DoubleLinkedList<T extends Comparable<T>> extends AbstractList<T>
{
	
	/**
	 * Atributo que indica el nodo para recorrer los elementos. 
	 * (B�sicamente un iterador de otra forma).
	 */
	private DoubleNode<T> listingNode;
	
	/**
	 * Nodo de la cola.
	 */
	private DoubleNode<T> lastNode;
	
	 /**
     * Construye una lista vacia
     * <b>post:< /b> se ha inicializado el primer nodo en null
     */
	public DoubleLinkedList() 
	{
		firstNode = null;
		listingNode = null;
		lastNode = null;
		size = 0;
	}
	
	/**
     * Se construye una nueva lista cuyo primer nodo  guardar� al elemento que llega por par�mentro
     * @param nFirst el elemento a guardar en el primer nodo
     * @throws NullPointerException si el elemento recibido es nulo
     */
	public DoubleLinkedList(T nFirst)
	{
		if(nFirst == null)
		{
			throw new NullPointerException("Se recibe un elemento nulo");
		}
		firstNode = new DoubleNode<T>(nFirst);
		lastNode = (DoubleNode<T>) firstNode;
		listingNode = null;
		size = 1;
	}
	
	/**
     * Agrega un elemento al final de la lista
     * Se actualiza la cantidad de elementos.
     * @param e el elemento que se desea agregar.
     * @return true en caso que se agregue el elemento o false en caso contrario. 
     * @throws NullPointerException si el elemento es nulo
     */
	@Override
	public boolean addLast(T element) 
	{
		boolean agregado = false;
		if(element == null)
		{
			throw new NullPointerException();
		}
		if(this.isEmpty())
		{
			this.firstNode = new DoubleNode<T>(element);
			this.lastNode = (DoubleNode<T>) this.firstNode;
			this.size++;
			agregado = true;
		}
		else
		{
			DoubleNode<T> n = new DoubleNode<T>(element);
			this.lastNode.setNext(n);
			n.setPrevious(this.lastNode);
			this.size++;
			this.lastNode = n;
			agregado = true;
		}
		return agregado;		
	}
	
	/**
	 * M�todo que a�ade un elemento a la lista
	 * @param element Elemento a a�adir.
	 * @return true si se a�adio, false de lo contrario.
	 */
	public boolean addFirst(T element)
	{
		boolean added = false;
		if(element == null)
		{
			throw new NullPointerException();
		}
		if(this.isEmpty())
		{
			this.firstNode = new DoubleNode<T>(element);
			this.lastNode = (DoubleNode<T>) this.firstNode;
			this.size++;
			added = true;
		}
		else
		{
			DoubleNode<T> n = new DoubleNode<T>(element);
			n.setNext(this.firstNode);
			((DoubleNode<T>)this.firstNode).setPrevious(n);
			this.size++;
			this.firstNode = n;
			added = true;
		}
		return added;
	}
	
	@Override
	public boolean delete(Object o) 
	{
		boolean elimino = false;
		boolean encontro = false;
		
		DoubleNode<T> n = (DoubleNode<T>) this.firstNode;
		while(n != null && ! encontro)
		{
			if(n.getElement().compareTo((T)o) == 0)
			{
				encontro = true;
			}
			else
			{		
				n = (DoubleNode<T>) n.getNextNode();
			}
		}
		
		if(encontro)
		{
			DoubleNode<T> anterior = n.getPrevious();
			if(anterior == null)
			{
				this.firstNode = n.getNextNode();
				if(firstNode != null)
				{
					((DoubleNode<T>)this.firstNode).setPrevious(null);
				}
			}
			else 
			{
				if (n.getNextNode() != null)
				{
					anterior.setNext(n.getNextNode());
					((DoubleNode<T>)n.getNextNode()).setPrevious(anterior);
				}
				else
				{
					anterior.setNext(n.getNextNode());
				}
			}
			elimino = true;
			this.size--;
		}
		return elimino;	
	}

	@Override
	public T get(Object o) 
	{
		DoubleNode<T> n = (DoubleNode<T>)this.firstNode;
		T searched = null;
		boolean found = false;
		if(n != null)
		{
			while(n != null && !found)
			{
				if(n.getElement().compareTo((T)o) == 0)
				{
					found = true;
					searched = n.getElement();
				}
				else
				{
					n = (DoubleNode<T>) n.getNextNode();
				}
			}
		}
		return searched;
	}
	
	@Override
	public void listing() 
	{
		listingNode = (DoubleNode<T>) this.firstNode;
		
	}

	@Override
	public T getCurrent() 
	{
		return listingNode.getElement();
	}

	@Override
	public boolean next() 
	{
		boolean exists = false;
		if(listingNode != null)
		{
			exists = true;
			listingNode = (DoubleNode<T>) listingNode.getNextNode();
		}
		return exists;
	}

}
