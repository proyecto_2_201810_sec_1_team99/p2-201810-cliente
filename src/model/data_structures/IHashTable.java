package model.data_structures;

import java.util.Iterator;

public interface IHashTable<Key,Value> 
{
	/**
	 * Agrega una dupla (K, V) a la tabla. Si la llave K existe, se reemplaza su valor V asociado. V no puede ser null.
	 * @param key
	 * @param value
	 */
	public void put(Key pKey, Value pValue);
	/**
	 * Obtener el valor V asociado a la llave K. V no puede ser null
	 * @param key
	 * @return
	 */
	public Value get(Key pKey);
	/**
	 * Borrar la dupla asociada a la llave K. Se obtiene el valor V asociado a la llave K. Se obtiene null si la llave K no existe
	 * @param key
	 * @return
	 */
	public Value delete(Key pKey);
	/*
	 * Conjunto de llaves K presentes en la tabla. 
	 */
	public Iterable<Key> keys();
	/**
	 * Aumenta la capacidad de la tabla.
	 */
	public void rehash(int pCap);
	/**
	 * Obtiene el codigo relacionado a la llave.
	 */
	public int getHashCode(Key pKey);
}
