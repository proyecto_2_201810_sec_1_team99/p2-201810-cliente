package model.logic;

import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import API.ITaxiTripsManager;
import jdk.internal.org.objectweb.asm.commons.SerialVersionUIDAdder;
import model.data_structures.BinaryTree;
import model.data_structures.DoubleLinkedList;
import model.data_structures.HashTableSC;
import model.data_structures.IList;
import model.data_structures.LinkedList;
import model.data_structures.MaxPQ;
import model.data_structures.SingleLinkedList;
import model.vo.Compania;
import model.vo.Servicio;
import model.vo.Taxi;
import model.vo.TaxiConPuntos;
import model.vo.TaxiConServicios;

public class TaxiTripsManager implements ITaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";
	
	private LinkedList<Taxi> taxis;
	private BinaryTree<Double, SingleLinkedList<Servicio>> arbolServicios;
	private BinaryTree<String, SingleLinkedList<Servicio>> arbolServiciosEnFecha;
	private LinkedList<Servicio> servicios;
	private LinkedList<Compania> companias;
	private HashTableSC<String, BinaryTree<String, SingleLinkedList<Servicio>>> hashServiciosZonas;
	private BinaryTree<String, Compania> arbolCompanias;
	private HashTableSC<Integer, SingleLinkedList<Servicio>> hashServiciosRangoDuracion;
	private HashTableSC<Double[], BinaryTree<String, SingleLinkedList<Servicio> >> hashServiciosLocalizacionGeo;
	private BinaryTree<String, SingleLinkedList<Servicio>> arbolServiciosEnHarvesiana;
	private BinaryTree<String, HashTableSC<String, SingleLinkedList<Servicio>>> serviciosPorFechaMinutos;
	private int tax;
	private Double[] localizacionReferencia;
	private HashTableSC<Double, BinaryTree<String, SingleLinkedList<Servicio>>> servicios2C;
	private double referenciaLo = 0;
	private double referenciaLa = 0;
	BinaryTree<Double, SingleLinkedList<Servicio>> serviciosEnArbol;
	
	@SuppressWarnings("unused")
	@Override
	public boolean cargarSistema(String direccionJson) 
	{
		System.out.println("hola");
		BinaryTree<String, SingleLinkedList<Servicio>> serviciosFechaEnArbol = new BinaryTree<>();
		HashTableSC<String, BinaryTree<String, SingleLinkedList<Servicio>>> serviciosZonasHash = new HashTableSC<>();
		localizacionReferencia = new Double[2];
		

		
		boolean cargo = false;
		if(servicios == null)
		{
			servicios = new SingleLinkedList<Servicio>( );
		}
		if(companias == null)
		{
			companias = new DoubleLinkedList<Compania>( );
		}
		if(arbolCompanias == null)
		{
			arbolCompanias = new BinaryTree<String, Compania>( );
		}
		if(serviciosPorFechaMinutos == null)
		{
			serviciosPorFechaMinutos = new BinaryTree<>( );
		}
		if(hashServiciosRangoDuracion == null)
		{
			hashServiciosRangoDuracion = new HashTableSC<>( );
		}
		if(arbolServicios == null)
		{
			arbolServicios = new BinaryTree<Double, SingleLinkedList<Servicio>>( );
		}
		if(arbolServiciosEnFecha == null)
		{
			arbolServiciosEnFecha = new BinaryTree<String, SingleLinkedList<Servicio>>( );
		}
		if(hashServiciosZonas == null)
		{
			hashServiciosZonas = new HashTableSC<String, BinaryTree<String, SingleLinkedList<Servicio>>>( );
		}
		if(servicios2C == null)
		{
			servicios2C = new HashTableSC<>( );
		}
		if(arbolServicios == null)
			{
				arbolServicios = new BinaryTree<Double, SingleLinkedList<Servicio>>( );
			}
		
		arbolServiciosEnFecha = new BinaryTree<String, SingleLinkedList<Servicio>>( );
		hashServiciosZonas = new HashTableSC<String, BinaryTree<String, SingleLinkedList<Servicio>>>( );
		hashServiciosLocalizacionGeo = new HashTableSC<Double[], BinaryTree<String, SingleLinkedList<Servicio>>>();
		serviciosEnArbol = new BinaryTree<>();
		
		String jCompany;
		String jDropoffCommunityArea;
		String jPickupCommunityArea;
		String jTaxiId;
		String jTripStartTimestamp;
		String jTripEndTimestamp;
		String jTripId;
		double jTripMiles;
		int jTripSeconds;
		double jTripTotal;
		double jPickupCentroidLatitude;
		double jPickupCentroidLongitude;
		

		JsonParser parser = new JsonParser( );
		try
		{
			JsonArray jServices = (JsonArray) parser.parse(new FileReader(direccionJson));

			for(int i = 0; jServices != null && i < jServices.size( ); i ++)
			{
				JsonObject jobj = jServices.get(i).getAsJsonObject( );

				if(jobj.get("company") == null)
				{
					jCompany = "Independiente";
				}
				else
				{
					jCompany = jobj.get("company").getAsString( );
				}

				if(jobj.get("dropoff_community_area") == null)
				{
					jDropoffCommunityArea = "N/A";
				}
				else
				{
					jDropoffCommunityArea = jobj.get("dropoff_community_area").getAsString( );
				}

				if(jobj.get("pickup_community_area") == null)
				{
					jPickupCommunityArea = "N/A";
				}
				else
				{
					jPickupCommunityArea = jobj.get("pickup_community_area").getAsString( );
				}

				if(jobj.get("taxi_id") == null)
				{
					jTaxiId = "N/A";
				}
				else
				{
					jTaxiId = jobj.get("taxi_id").getAsString( );
				}

				if(jobj.get("trip_start_timestamp") == null)
				{
					jTripStartTimestamp = "N/A";
				}
				else
				{
					jTripStartTimestamp = jobj.get("trip_start_timestamp").getAsString();
				}

				if(jobj.get("trip_end_timestamp") == null)
				{
					jTripEndTimestamp = "N/A";
				}
				else
				{
					jTripEndTimestamp = jobj.get("trip_end_timestamp").getAsString();
				}

				if(jobj.get("trip_id") == null)
				{
					jTripId = "N/A";
				}
				else
				{
					jTripId = jobj.get("trip_id").getAsString( );
				}

				if(jobj.get("trip_miles") == null)
				{
					jTripMiles = 0;
				}
				else
				{
					jTripMiles = jobj.get("trip_miles").getAsDouble( );
				}

				if(jobj.get("trip_seconds") == null)
				{
					jTripSeconds = 0;
				}
				else
				{
					jTripSeconds = jobj.get("trip_seconds").getAsInt( );
				}

				if(jobj.get("trip_total") == null)
				{
					jTripTotal = 0;
				}
				else
				{
					jTripTotal = jobj.get("trip_total").getAsDouble( );
				}
				if(jobj.get("pickup_centroid_latitude") == null)
				{
					jPickupCentroidLatitude = 0;
				}
				else
				{
					jPickupCentroidLatitude = jobj.get("pickup_centroid_latitude").getAsDouble();
				}
				if(jobj.get("pickup_centroid_longitude") == null)
				{
					jPickupCentroidLongitude = 0;
				}
				else
				{
					jPickupCentroidLongitude = jobj.get("pickup_centroid_longitude").getAsDouble( );
				}

				Servicio jServicio = new Servicio(jDropoffCommunityArea, jPickupCommunityArea, jTaxiId, jTripId, jTripMiles, jTripSeconds, jTripTotal, jTripStartTimestamp, jTripEndTimestamp, jPickupCentroidLatitude, jPickupCentroidLongitude);
				servicios.addLast(jServicio);
			
				//Conformaci�n de grupos en HashTable<distancia, lista<Servicio>>
				SingleLinkedList<Servicio> listaServEnFecha = arbolServiciosEnFecha.get(jTripStartTimestamp);
				if(listaServEnFecha == null)
				{
					SingleLinkedList<Servicio> listaServiciosFecha = new SingleLinkedList<>();
					listaServiciosFecha.addLast(jServicio);
					arbolServiciosEnFecha.put(jTripStartTimestamp, listaServiciosFecha);
					serviciosZonasHash.put(jPickupCommunityArea+"-"+jDropoffCommunityArea, arbolServiciosEnFecha);
				}
				else if(listaServEnFecha != null)
				{
					listaServEnFecha.addLast(jServicio);  
				}
				
				
				//Crear lista de companias
				Compania c = getCompany(jCompany);
				
				if(c == null)
				{
					c = new Compania(jCompany);
					companias.addLast(c);
					arbolCompanias.put(jCompany, c);				//Agregar compa�ia a un arbol balanceado
					Taxi t = new Taxi(jTaxiId, jCompany);
					t.addServicio(jServicio);
					t.addMiles(jTripMiles);
					t.addMoney(jTripTotal);
					c.addTaxi(t);
					tax++;
				}
				else
				{
					Taxi t = c.getTaxi(jTaxiId);
					if(t == null)
					{
						t = new Taxi(jTaxiId, jCompany);
						t.addServicio(jServicio);
						t.addServicioPickUp(jServicio);
						t.addMiles(jTripMiles);
						t.addMoney(jTripTotal);
						c.addTaxi(t);
						tax++;
					}
					else
					{
						t.addServicio(jServicio);
						t.addServicioPickUp(jServicio);
						t.addMiles(jTripMiles);
						t.addMoney(jTripTotal);
					}
				}

				c.addServiciosDeCompania(jServicio);		

				
				//Hash table agrupando los servicios por rango
				int llave = (int) Math.floor((jTripSeconds/60)-0.0001);
				
				if(!hashServiciosRangoDuracion.contains(llave))
				{
					SingleLinkedList<Servicio> serviciosRangoDuracion = new SingleLinkedList<Servicio>( );
					serviciosRangoDuracion.addLast(jServicio);
					hashServiciosRangoDuracion.put(llave, serviciosRangoDuracion);					
				}
				else
				{
					SingleLinkedList<Servicio> s = hashServiciosRangoDuracion.get(llave);
					s.addLast(jServicio);
					hashServiciosRangoDuracion.put(llave, s);
				}
				
				
				//Agrupacion de servicios por fecha y cada 15 minutos (3C)
				 String [] fechas = jTripStartTimestamp.split("T");
				 String fecha = fechas[0];
				 String hora = fechas[1];
				 
				 if(!serviciosPorFechaMinutos.contains(fecha))
				 {
					 HashTableSC<String, SingleLinkedList<Servicio>> serviciosPor15Minutos = new HashTableSC<>( );
					 serviciosPorFechaMinutos.put(fecha, serviciosPor15Minutos);
					 if(!serviciosPor15Minutos.contains(hora))
					 {
						 SingleLinkedList<Servicio> servicios3C = new SingleLinkedList<>();
						 servicios3C.addLast(jServicio);
						 serviciosPor15Minutos.put(hora, servicios3C);
					 }
					 else
					 {
						 SingleLinkedList<Servicio> s = serviciosPor15Minutos.get(hora);
						 s.addLast(jServicio);
						 serviciosPor15Minutos.put(hora, s);
					 }
				 }
				 else
				 {
					 HashTableSC<String, SingleLinkedList<Servicio>> sPor15Minutos = serviciosPorFechaMinutos.get(fecha);
					 if(!sPor15Minutos.contains(hora))
					 {
						 SingleLinkedList<Servicio> servicios3C = new SingleLinkedList<>();
						 servicios3C.addLast(jServicio);
						 sPor15Minutos.put(hora, servicios3C);
					 }
					 else
					 {
						 SingleLinkedList<Servicio> s = sPor15Minutos.get(hora);
						 s.addLast(jServicio);
						 sPor15Minutos.put(hora, s);
					 }
				 }
				
				//Conformaci�n de grupos en arbol<distancia,lista<Servicios>>
				if(jTripMiles > 0)
				{
					double  tripMiles = (Math.round(jTripMiles*10))/10;
					//String[] stringMiles = Double.toString(Math.abs(jServicio.getTripMiles())).split(".");
					//String miles = stringMiles[0]+"."+stringMiles[1].substring(0, 1);
					//double tripMiles = Double.parseDouble(miles);
					if(!serviciosEnArbol.contains(tripMiles))
					{
						SingleLinkedList<Servicio> listaServicios = new SingleLinkedList<>();
						listaServicios.addLast(jServicio);
						serviciosEnArbol.put(tripMiles, listaServicios);
					}
					else
					{
						SingleLinkedList<Servicio> listaSrv = serviciosEnArbol.get(tripMiles);
						listaSrv.addLast(jServicio);
						serviciosEnArbol.put(tripMiles, listaSrv);
					}
				}	
				 
			}
			//2C
			for(Servicio s : servicios)
			{
				if(s.getPickupCentroidLatitude() != 0 ||  s.getPickupCentroidLongitude() != 0)
				{
					referenciaLa += s.getPickupCentroidLatitude();
					referenciaLo += s.getPickupCentroidLongitude();
				}
			}
			referenciaLo = referenciaLo/servicios.size();
			referenciaLa = referenciaLa/servicios.size();
			double llave = 0;
			for(Servicio s : servicios)
			{
				llave = Math.floor((getDistance(s.getPickupCentroidLatitude(), s.getPickupCentroidLongitude(), referenciaLa, referenciaLo)*10))/10;
				if(!servicios2C.contains(llave))
				{
					BinaryTree<String, SingleLinkedList<Servicio>> nueva = new BinaryTree<>( );
					if(!nueva.contains(s.getTaxiId()))
					{
						SingleLinkedList<Servicio> lista = new SingleLinkedList<>( );
						lista.addLast(s);
						nueva.put(s.getTaxiId(), lista);
					}
					else
					{
						SingleLinkedList<Servicio> lista = nueva.get(s.getTaxiId());
						lista.addLast(s);
						nueva.put(s.getTaxiId(), lista);
					}
					servicios2C.put(llave, nueva);
				}
				else
				{
					BinaryTree<String, SingleLinkedList<Servicio>> nueva = servicios2C.get(llave);
					if(!nueva.contains(s.getTaxiId()))
					{
						SingleLinkedList<Servicio> lista = new SingleLinkedList<>( );
						lista.addLast(s);
						nueva.put(s.getTaxiId(), lista);
					}
					else
					{
						SingleLinkedList<Servicio> lista = nueva.get(s.getTaxiId());
						lista.addLast(s);
						nueva.put(s.getTaxiId(), lista);
					}
					servicios2C.put(llave, nueva);
				}
			}
			
			
			arbolServicios = serviciosEnArbol;
			
			serviciosFechaEnArbol = arbolServiciosEnFecha;
			serviciosZonasHash = hashServiciosZonas;

			cargo = true;
			System.out.println("Se cargaron los datos de manera exitosa");
			System.out.println("Se Adicionaron " + servicios.size() + " Servicios.\n"
					+ "Se Adicionaron " + companias.size() + " Companias (los independientes se agrupan como una compania).\n"
					+ "Se Adicionaron " + tax + " Taxis." + "\n");
		}
		catch(Exception e)
		{
			//System.out.println(e.getMessage());
			System.out.println("*SOMETHING WRONG HAPPENED*");
			e.printStackTrace();
		}
		return cargo;
	}
	
	
	public Compania getCompany(String name)
	{
		Compania c = null;
		Iterator<Compania> it = companias.iterator();
		boolean found = false;

		while(!found && it.hasNext())
		{
			Compania x = it.next();
			if(x.getNombre().equals(name))
			{
				c = x;
				found = true;
			}
		}
		return c;
	}
	

	@SuppressWarnings("unused")
	@Override
	public LinkedList<Taxi> A1TaxiConMasServiciosEnZonaParaCompania(int zonaInicio, String compania) 
	{
		Compania c = arbolCompanias.get(compania);
		
		SingleLinkedList<Taxi> taxisSolicitados = new SingleLinkedList<>( );
		for(Taxi t : c.getTaxisInscritos())
		{
			if(taxisSolicitados.size( ) == 0)
			{
				taxisSolicitados.addFirst(t);
			}
			else if(t.getNumServiciosPickUp() > taxisSolicitados.get(0).getNumServiciosPickUp())
			{
				while(!taxisSolicitados.isEmpty())
				{
					int i = 0;
					taxisSolicitados.delete(taxisSolicitados.get(0));
				}
				taxisSolicitados.addFirst(t);
			}
			else if(t.getNumServiciosPickUp() == taxisSolicitados.get(0).getNumServiciosPickUp())
			{
				taxisSolicitados.addLast(t);
			}
		}
		
		System.out.println("Taxis con servicios empezados en la zona " + zonaInicio + ":");
		for(Taxi t : taxisSolicitados)
		{
			System.out.println("ID del taxi: " + t.getTaxiId( ) + "\n" +"Hora y fecha de inicio del sus servicios:");
			for(Servicio s : t.getServiciosAsociados())
			{
				System.out.println(s.getStartTimestamp());
			}
		}
		
		return taxisSolicitados;
	}


	@Override
	public LinkedList<Servicio> A2ServiciosPorDuracion(int duracion) 
	{
		int llave = (int)Math.floor((duracion/60) - 0.0001);
		SingleLinkedList<Servicio> serviciosSolicitados = new SingleLinkedList<>( );
		for(Servicio s : hashServiciosRangoDuracion.get(llave))
		{
			serviciosSolicitados.addLast(s);
			System.out.println("ID del servicio: " + s.getTripId() + "\n"
					+ "ID del taxi: " + s.getTaxiId() + "\n"
					+ "duraci�n del servicio: " + s.getTripSeconds() + " segundos" + "\n");
		}
		
		return serviciosSolicitados;
	}

	@SuppressWarnings("unchecked")
	@Override
	public IList<Servicio> B1ServiciosPorDistancia(double distanciaMinima, double distanciaMaxima) 
	{
		SingleLinkedList<Servicio> listaServicios = new SingleLinkedList<Servicio>();
		Iterator<Double> it = arbolServicios.keys().iterator();
		
		while(it.hasNext())
		{
			Double distanciaServicio = it.next();
			if(distanciaServicio > distanciaMinima && distanciaServicio <= distanciaMaxima)
				listaServicios = arbolServicios.get(distanciaServicio);
		}
		return (IList<Servicio>) listaServicios;
	}

	@SuppressWarnings("unchecked")
	@Override
	public IList<Servicio> B2ServiciosPorZonaRecogidaYLlegada(int zonaInicio, int zonaFinal, String fechaI, String fechaF, String horaI, String horaF) 
	{
		Date fechaInicio;
		Date fechaFin;
		String idZonaInicio;
		String idZonaFin;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		SingleLinkedList<Servicio> listaServiciosZonaPickDrop =  new SingleLinkedList<Servicio>();
		BinaryTree<String, SingleLinkedList<Servicio>> arbolAux = new BinaryTree<>();
		
		try
		{
			idZonaInicio = Integer.toString(zonaInicio);
			idZonaFin = Integer.toString(zonaFinal);
			fechaInicio = sdf.parse(fechaI+"T"+horaI);
			fechaFin = sdf.parse(fechaF+"T"+horaF);
			
			String zonaIzonaF = idZonaInicio+"-"+idZonaFin;
			arbolAux = hashServiciosZonas.get(zonaIzonaF);
			Iterator<String> it = (Iterator<String>) arbolAux.keys();
			while(it.hasNext()) 
			{
				String fechaCompare = it.next();
				Date fechaComparar = sdf.parse(fechaCompare);
				if(fechaComparar.compareTo(fechaInicio) >= 0 && fechaComparar .compareTo(fechaFin) <= 0)
					listaServiciosZonaPickDrop = arbolAux.get(fechaCompare);
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		return (IList<Servicio>) listaServiciosZonaPickDrop;
	}

	@Override
	public TaxiConPuntos[] R1C_OrdenarTaxisPorPuntos() 
	{
		//Hacer operacones, a�adirlas a una cola de prioridad y hacer el heapsort
		TaxiConPuntos[] taxisConPuntos = null;
		TaxiConPuntos taxiPuntos = null;
		ArrayList<Double> prioridadPuntos = new ArrayList<>();

		//PQ<TaxiConPuntos, Double> maxPQ = new PQ<>();

		MaxPQ<Double> pq = new MaxPQ<>();
		double puntos = 0.0;
		for(Taxi taxi : taxis)
		{
			double numServicios = taxi.getServiciosAsociados().size();
			for(Servicio servicio : taxi.getServiciosAsociados())
			{
				double tripMiles = servicio.getTripMiles();
				double tripTotal = servicio.getTripTotal();
				if(tripMiles > 0.0 & tripTotal > 0.0)
					puntos = tripMiles + (tripTotal/tripMiles) * (numServicios);
				pq.insert(puntos);
			}	
			taxiPuntos = (TaxiConPuntos) taxi;
		}
		while(!pq.isEmpty())
			prioridadPuntos.add(pq.delMax());
		//Inconvemiente al tratar de agrgar los taxis al arreglo con sus puntos	
		return taxisConPuntos;
	}

	@Override
	public LinkedList<Servicio> R2C_LocalizacionesGeograficas(String taxiIDReq2C, double millasReq2C) 
	{
		SingleLinkedList<Servicio> respuesta = servicios2C.get(millasReq2C).get(taxiIDReq2C);
		if(respuesta == null)
		{
			System.out.println("El taxi solicitado no tiene servicios a esta distancia");
		}
		
		System.out.println("ID del taxi: " + taxiIDReq2C);
		for(Servicio s : respuesta)
		{
			System.out.println("ID de servivio: " + s.getTripId() + "\n" + 
								"Localizaci�n de inicio: " + s.getPickupCentroidLatitude() + " - " + s.getPickupCentroidLongitude() + "\n" + 
								"Distancia a la localizaci�n de referencia: " + getDistance(s.getPickupCentroidLatitude(), s.getPickupCentroidLongitude(), referenciaLa, referenciaLo) + "\n");
		}
		
		
		return respuesta;
	}

	@Override
	public LinkedList<Servicio> R3C_ServiciosEn15Minutos(String fecha, String hora) 
	{
		String [] horas = hora.split(":");
		
		double min = Double.parseDouble(horas[1]);
		double r = min%15;
		
		String horaMayor = horas[0] + ":" + ((int)min-r+15) + ":" + horas[2];
		String horaMenor = horas[0] + ":" + ((int)min-r) + ":" + horas[2];
		
		SingleLinkedList<Servicio> s = new SingleLinkedList<>( );
		System.out.println(r);
		System.out.println(horaMayor);
		System.out.println(horaMenor);
		if(r >= 7.5)
		{
			serviciosPorFechaMinutos.get(fecha);
			for(Servicio ser : serviciosPorFechaMinutos.get(fecha).get(horaMayor))
			{
				if(ser.getPickCommunityArea() != ser.getDropCommunityArea())
				{
					s.addLast(ser);
				}
			}
		}
		else
		{
			for(Servicio ser : serviciosPorFechaMinutos.get(fecha).get(horaMenor))
			{
				if(ser.getPickCommunityArea() != ser.getDropCommunityArea())
				{
					s.addLast(ser);
				}
			}
		}
		
		for(Servicio ser : s)
		{
			System.out.println(ser.getTripId());
		}
		
		return s;
	}
	
	public double getDistance(double lat1, double lon1, double lat2, double lon2)
	{
		final int R = 6371*1000;
		double latDistance = Math.toRadians(lat2 - lat1);
		double lonDistance = Math.toRadians(lon2 - lon1);
		double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(Math.toRadians(lat1))
				   * Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double distance = R * c;
		return distance;	
	}
}
