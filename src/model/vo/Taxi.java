package model.vo;

import model.data_structures.HashTableSC;
import model.data_structures.LinkedList;
import model.data_structures.SingleLinkedList;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{
	private String taxi_id;
	private String company;
	private LinkedList<Servicio> serviciosAsociados;
	private HashTableSC<String , SingleLinkedList<Servicio>> hashServiciosPickUp;
	private int numServicesInRange;
	private double moneyInRange;
	private double milesTotal;	
	private double moneyTotal;
	private int numServiciosPickUp;
	private SingleLinkedList<Servicio> serviciosPickUp;
	
	
	public Taxi(String pTaxi, String pCompany)
	{
		taxi_id = pTaxi;
		company = pCompany;
		serviciosAsociados = new SingleLinkedList<Servicio>();
		milesTotal = 0;
		moneyTotal = 0;
		numServiciosPickUp = 0;
		serviciosPickUp = null;
		hashServiciosPickUp = new HashTableSC<>();
	}
	
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi_id;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}
	
	public boolean addServicio(Servicio s)
	{
		return serviciosAsociados.addLast(s);
	}
	
	public LinkedList<Servicio> getServiciosAsociados()
	{
		return serviciosAsociados;
	}
	
	public HashTableSC<String, SingleLinkedList<Servicio>> getServiciosPickUp( )
	{
		return hashServiciosPickUp;
	}
	
	public void addServicioPickUp(Servicio s)
	{
		if(!hashServiciosPickUp.contains(s.getPickCommunityArea()))
		{
			serviciosPickUp = new SingleLinkedList<Servicio>();
			serviciosPickUp.addLast(s);
			hashServiciosPickUp.put(s.getPickCommunityArea(), serviciosPickUp);
			numServiciosPickUp ++;
		}
		else
		{
			hashServiciosPickUp.get(s.getPickCommunityArea()).addLast(s);
			hashServiciosPickUp.put(s.getPickCommunityArea( ), serviciosPickUp);
			numServiciosPickUp ++;
		}
	}
	
	@Override
	public int compareTo(Taxi o) 
	{
		int ans = 0;
		if(getTaxiId().compareTo(o.getTaxiId()) < 0)
		{
			ans = -1;
		}	
		else if (getTaxiId().compareTo(o.getTaxiId()) > 0)
		{
			ans = 1;
		}
		
		return ans;
	}

	public int getNumServicesInRange() 
	{
		return numServicesInRange;
	}
	
	public int getNumServiciosPickUp( )
	{
		return numServiciosPickUp;
	}
	
	public void setNumServicesInRange(int numServicesInRange)
	{
		this.numServicesInRange = numServicesInRange;
	}
	
	public void addMiles(double nMiles)
	{
		milesTotal += nMiles;
	}
	
	public void addMoney(double money)
	{
		moneyTotal += money;
	}
	
	public double getRentabilidad()
	{
		return (milesTotal != 0) ? (moneyTotal/milesTotal) : 0;
	}
	
	@Override
	public String toString()
	{
		return taxi_id;
	}

	public double getMoneyInRange() 
	{
		return moneyInRange;
	}

	public void setMoneyInRange(double moneyInRange) 
	{
		this.moneyInRange = moneyInRange;
	}
}