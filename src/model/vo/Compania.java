package model.vo;

import java.util.Iterator;

import model.data_structures.LinkedList;
import model.data_structures.SingleLinkedList;

public class Compania implements Comparable<Compania> 
{
	
	private String nombre;
	
	private LinkedList<Taxi> taxisInscritos;
	
	private int numServices;
	
	private LinkedList<Servicio> serviciosDeCompania;
	
	public Compania(String nombre)
	{
		this.setNombre(nombre);
		taxisInscritos = new SingleLinkedList<Taxi>();
		numServices = 0;
		setServiciosDeCompania(new SingleLinkedList<>());
	}
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedList<Taxi> getTaxisInscritos() {
		return taxisInscritos;
	}

	public void setTaxisInscritos(LinkedList<Taxi> taxisInscritos) 
	{
		this.taxisInscritos = taxisInscritos;
	}
	
	public boolean addTaxi(Taxi t)
	{
		return taxisInscritos.addLast(t);
	}

	public Taxi getTaxi(String id)
	{
		Iterator<Taxi> it = taxisInscritos.iterator();
		boolean found = false;
		Taxi t = null;
		while(!found && it.hasNext())
		{
			Taxi x = it.next();
			if(x.getTaxiId().equals(id))
			{
				t = x;
				found = true;
			}
		}
		return t;
	}
	
	public Taxi getTaxiMasRentable()
	{
		taxisInscritos.listing();
		Taxi top = taxisInscritos.getCurrent();
		for (Taxi t : taxisInscritos) 
		{	
			if(t.getRentabilidad() > top.getRentabilidad())
			{
				top = t;
			}
		}
		return top;
	}
	
	public int getNumServices()
	{
		return numServices;
	}
	
	@Override
	public int compareTo(Compania o) 
	{
		int ans = 0;
		if(this.nombre.compareTo(o.getNombre()) < 0)
		{
			ans = -1;
		}
		else if (this.nombre.compareTo(o.getNombre()) > 0)
		{
			ans = 1;
		}
		return ans;
	}
	
	public LinkedList<Servicio> getServiciosDeCompania() 
	{
		return serviciosDeCompania;
	}
	public void setServiciosDeCompania(LinkedList<Servicio> serviciosDeCompania) 
	{
		this.serviciosDeCompania = serviciosDeCompania;
	}
	
	public void addServiciosDeCompania(Servicio s)
	{
		serviciosDeCompania.addLast(s);
		numServices += 1;
	}
}
