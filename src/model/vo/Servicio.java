package model.vo;

/**
 * Representation of a Service object
 */
public class Servicio implements Comparable<Servicio>
{	
	private String dropoff_community_area;
	private String taxi_id;
	private String trip_id;
	private double trip_miles;
	private int trip_seconds;
	private double trip_total;
	private String trip_start_timestamp;
	private String trip_end_timestamp;
	private String pickup_community_area;
	private RangoDistancia rangoDistancia;
	private double pickup_centroid_latitude;
	private double pickup_centroid_longitude;
	
	
	public Servicio(String pCommunity,String pCom2, String pTaxi, String pTrip, double pMiles, int pSeconds, double pTotal, String startTimestamp, String endTimestamp, double pCentroidLatitude, double pCentroidLongitude)
	{
		dropoff_community_area = pCommunity;
		pickup_community_area =  pCom2;
		taxi_id = pTaxi;
		trip_id = pTrip;
		trip_miles = pMiles;
		trip_seconds = pSeconds;
		trip_total = pTotal;
		trip_start_timestamp = startTimestamp;
		trip_end_timestamp = endTimestamp;
		pickup_centroid_latitude = pCentroidLatitude;
		pickup_centroid_longitude = pCentroidLongitude;
	}
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return trip_id;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi_id;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return trip_seconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return trip_miles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return trip_total;
	}
	
	public double getPickupCentroidLatitude()
	{
		return pickup_centroid_latitude;
	}
	
	public double getPickupCentroidLongitude()
	{
		return pickup_centroid_longitude;
	}

	@Override
	public int compareTo(Servicio o) 
	{
		// TODO Auto-generated method stub
		int ans = 0;
		if(trip_id.compareTo(o.getTripId()) > 0)
		{
			ans = 1;
		}
		else if (trip_id.compareTo(o.getTripId()) < 0)
		{
			ans = -1;
		}
		return ans;
	}
	
	public String getDropCommunityArea( )
	{
		return dropoff_community_area;
	}
	
	public String getPickCommunityArea( )
	{
		return pickup_community_area;
	}

	public String getStartTimestamp() {
		return trip_start_timestamp;
	}
	
	public String getEndTimestamp() {
		return trip_end_timestamp;
	}
	
	public RangoDistancia getRango() {
		return rangoDistancia;
	}
	
	public boolean estaEnRango(RangoFechaHora rango)
	{
		boolean is = false;
		boolean statement1 = this.getStartTimestamp().compareTo(rango.getFechaInicial() + "T" + rango.getHoraInicio()) >= 0;
		boolean statement2 = this.getEndTimestamp().compareTo(rango.getFechaFinal() + "T" + rango.getHoraFinal()) <= 0;
		if(statement1 && statement2)
		{
			is = true;
		}

		return is;
	}

}
